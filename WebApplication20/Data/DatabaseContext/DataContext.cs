﻿using DocumentFormat.OpenXml.Presentation;
using Microsoft.EntityFrameworkCore;
using WebApplication20.Models.Database;

namespace WebApplication20.Data.DatabaseContext
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Goods> Goods { get; set; }
        public DbSet<UserData> UsersData { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<ManufacturerGoods> ManufacturerGoods { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
    }
}
