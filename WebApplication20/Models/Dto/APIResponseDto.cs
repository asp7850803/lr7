﻿using System.Collections.Generic;
using System;
using System.Text.Json.Serialization;


namespace WebApplication20.Models.Dto
{
    public class Meta
    {
        [JsonPropertyName("code")]
        public int Code { get; set; }
        [JsonPropertyName("disclaimer")]
        public string Disclaimer { get; set; }
    }

    public class Response
    {
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        [JsonPropertyName("base")]
        public string Base { get; set; }
        [JsonPropertyName("rates")]
        public Dictionary<string, double> Rates { get; set; }
    }

    public class APIResponseDto
    {
        [JsonPropertyName("meta")]
        public Meta Meta { get; set; }
        [JsonPropertyName("response")]
        public Response Response { get; set; }
        [JsonPropertyName("date")]
        public DateTime Date { get; set; }
        [JsonPropertyName("base")]
        public string Base { get; set; }
        [JsonPropertyName("rates")]
        public Dictionary<string, double> Rates { get; set; }
    }
}
