﻿namespace WebApplication20.Models.Dto
{
    public class CreateGoodDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
