﻿using System.Net;

namespace WebApplication20.Models.Dto
{
    public class ResponseDto<T>
    {
        public string Message { get; set; }
        public List<T> Data { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
