﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace WebApplication20.Models.Dto
{
    public class CreateUserDto
    {
        [StringLength(15, MinimumLength = 1, ErrorMessage = "Username must be between 1 and 15 characters")]
        public string FirstName { get; set; }
        [StringLength(15, MinimumLength = 1, ErrorMessage = "Username must be between 1 and 15 characters")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
