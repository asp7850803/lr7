﻿namespace WebApplication20.Models.Dto
{
    public class AuthResponseDto
    {
        public string Token { get; set; }

        public AuthResponseDto(string token)
        {
            Token = token;
        }
    }
}
