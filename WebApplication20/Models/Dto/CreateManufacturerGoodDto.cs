﻿namespace WebApplication20.Models.Dto
{
    public class CreateManufacturerGoodDto
    {
        public int GoodId { get; set; }
        public int ManufacturerId { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }
}
