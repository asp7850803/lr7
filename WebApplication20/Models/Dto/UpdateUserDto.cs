﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication20.Models.Dto
{
    public class UpdateUserDto
    {
        public int Id { get; set; }
        [StringLength(15, MinimumLength = 1, ErrorMessage = "Username must be between 1 and 15 characters")]
        public string? FirstName { get; set; }
        [StringLength(15, MinimumLength = 1, ErrorMessage = "Username must be between 1 and 15 characters")]
        public string? LastName { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [Range(1, 2, ErrorMessage = "RoleId must be either 1 or 2.")]
        public int? RoleId { get; set; }
    }
}
