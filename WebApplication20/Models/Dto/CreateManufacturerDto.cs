﻿namespace WebApplication20.Models.Dto
{
    public class CreateManufacturerDto
    {
        public string Name { get; set; }
        public string Founder { get; set; }
        public string Country { get; set; }
    }
}
