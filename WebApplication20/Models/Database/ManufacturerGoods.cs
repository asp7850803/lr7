﻿namespace WebApplication20.Models.Database
{
    public class ManufacturerGoods
    {
        public int Id { get; set; }
        public int GoodId { get; set; }
        public int ManufacturerId { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }
}
