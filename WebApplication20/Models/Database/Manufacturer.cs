﻿namespace WebApplication20.Models.Database
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Founder { get; set; }
        public string Country { get; set; }
    }
}
