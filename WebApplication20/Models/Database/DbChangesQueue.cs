﻿namespace WebApplication20.Models.Database
{
    public class DbChangesQueue
    {
        public string TableName { get; set; }
        public int Id { get; set; }
    }
}
