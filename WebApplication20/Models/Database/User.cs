﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace WebApplication20.Models.Database
{
    public class UserData
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public byte[] PasswordHash {  get; set; }
        public byte[] PasswordSalt { get; set; }
        public DateTime LastAuthorize { get; set; }
        public int InvalidLoginAttempts { get; set; }
        public bool IsLocked { get; set; }
        public int RoleId { get; set; }
    }
}
