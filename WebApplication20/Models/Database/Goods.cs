﻿namespace WebApplication20.Models.Database
{
    public class Goods
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
