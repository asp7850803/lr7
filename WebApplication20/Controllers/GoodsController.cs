﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.GoodsService;

namespace WebApplication20.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GoodsController : ControllerBase
    {
        private readonly IGoodsService _goodsService;
        private readonly ILogger<GoodsController> _logger;

        public GoodsController(IGoodsService goodsService, ILogger<GoodsController> logger)
        {
            _goodsService = goodsService;
            _logger = logger;
        }

        /// <summary>
        ///     Get good by id.
        /// </summary>
        /// <param name="id">Good id</param>
        /// <returns></returns>
        [HttpGet("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Goods>> Get(int id)
        {
            return await this._goodsService.Get(id, Response);
        }

        /// <summary>
        ///     Get all goods.
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<Goods>> GetAll()
        {
            return await this._goodsService.GetAll(Response);
        }

        /// <summary>
        ///     Create good.
        /// </summary>
        /// <param name="dto">Good object</param>
        /// <returns></returns>
        [HttpPost, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<Goods>> Post(CreateGoodDto dto)
        {
            return await this._goodsService.Post(dto, Response);
        }


        /// <summary>
        ///    Update good.
        /// </summary>
        /// <param name="id">Good id</param>
        /// <param name="dto">Good object</param>
        /// <returns></returns>
        [HttpPut("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Goods>> Put(int id, UpdateGoodDto dto)
        {
            if (id != dto.Id)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<Goods>
                {
                    Data = new List<Goods>(),
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Id in url and id in body are not the same"
                };
            }
            return await this._goodsService.Put(dto, Response);
        }


        /// <summary>
        ///    Delete good.
        /// </summary>
        /// <param name="id">Good id</param>
        /// <returns></returns>
        [HttpDelete("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Goods>> Delete(int id)
        {
            return await this._goodsService.Delete(id, Response);
        }
    }
}
