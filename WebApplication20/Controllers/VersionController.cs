﻿using Asp.Versioning;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication20.Models.Dto;
using WebApplication20.Services.User;
using WebApplication20.Services.Version;

namespace WebApplication20.Controllers
{
    [ApiVersion(1.0, Deprecated = true)]
    [ApiVersion(2.0)]
    [ApiVersion(3.0)]
    [ApiController]
    [Route("api/[controller]/v{version:apiVersion}")]
    [Authorize]
    public class VersionController : ControllerBase
    {
        private readonly IVersionService _versionService;
        private readonly ILogger<VersionController> _logger;

        public VersionController(IVersionService versionService, ILogger<VersionController> logger)
        {
            _versionService = versionService;
            _logger = logger;
        }

        /// <summary>
        ///     Get random number.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [MapToApiVersion(1.0)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<int>> Get()
        {
            return await this._versionService.Get(Response);
        }

        /// <summary>
        ///     Get random string.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [MapToApiVersion(2.0)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<string>> GetV2()
        {
            return await this._versionService.GetV2(Response);
        }

        /// <summary>
        ///     Get excel file.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [MapToApiVersion(3.0)]
        [Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<FileResult> GetV3()
        {
            var excel =  await this._versionService.GetV3(Response);
            return File(excel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "sample.xlsx");
        }
    }
}
