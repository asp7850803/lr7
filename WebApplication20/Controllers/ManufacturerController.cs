﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.ManufacturerService;

namespace WebApplication20.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ManufacturerController : ControllerBase
    {
        private readonly IManufacturerService _manufacturerService;
        private readonly ILogger<ManufacturerController> _logger;

        public ManufacturerController(IManufacturerService manufacturerService, ILogger<ManufacturerController> logger)
        {
            _manufacturerService = manufacturerService;
            _logger = logger;
        }


        /// <summary>
        ///     Get manufacturer by id.
        /// </summary>
        /// <param name="id">Manufacturer id</param>
        /// <returns></returns>
        [HttpGet("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Manufacturer>> Get(int id)
        {
            return await this._manufacturerService.Get(id, Response);
        }

        /// <summary>
        ///     Get all manufacturers.
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<Manufacturer>> GetAll()
        {
            return await this._manufacturerService.GetAll(Response);
        }

        /// <summary>
        ///     Create manufacturer.
        /// </summary>
        /// <param name="dto">Manufacturer object</param>
        /// <returns></returns>
        [HttpPost, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<Manufacturer>> Post(CreateManufacturerDto dto)
        {
            return await this._manufacturerService.Post(dto, Response);
        }


        /// <summary>
        ///    Update manufacturer.
        /// </summary>
        /// <param name="id">Manufacturer id</param>
        /// <param name="dto">Manufacturer object</param>
        /// <returns></returns>
        [HttpPut("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Manufacturer>> Put(int id, UpdateManufacturerDto dto)
        {
            if (id != dto.Id)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<Manufacturer>
                {
                    Data = new List<Manufacturer>(),
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Id in url and id in body are not the same"
                };
            }
            return await this._manufacturerService.Put(dto, Response);
        }

        /// <summary>
        ///    Delete manufacturer.
        /// </summary>
        /// <param name="id">Manufacturer id</param>
        /// <returns></returns>
        [HttpDelete("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<Manufacturer>> Delete(int id)
        {
            return await this._manufacturerService.Delete(id, Response);
        }
    }
}
