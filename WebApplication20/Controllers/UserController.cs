﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.ManufacturerGoodsService;
using WebApplication20.Services.User;

namespace WebApplication20.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Roles ="Admin")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        /// <summary>
        ///     Get all users.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<UserData>> GetAll()
        {
            return await this._userService.GetAll(Response);
        }

        /// <summary>
        ///     Get user by id.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<UserData>> Get(int id)
        {
            return await this._userService.Get(id, Response);
        }

        /// <summary>
        ///    Delete user.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        [HttpDelete("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<UserData>> Delete(int id)
        {
            return await this._userService.Delete(id, Response);
        }

        /// <summary>
        ///    Update user.
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="dto">User object</param>
        /// <returns></returns>
        [HttpPut("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<UserData>> Put(int id, UpdateUserDto dto)
        {
            if (id != dto.Id)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<UserData>
                {
                    Data = new List<UserData>(),
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Id in url and id in body are not the same"
                };
            }
            return await this._userService.Put(dto, Response);
        }
    }
}
