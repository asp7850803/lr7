﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.ManufacturerGoodsService;

namespace WebApplication20.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ManufacturerGoodsController : ControllerBase
    {
        private readonly IManufacturerGoodsService _manufacturerGoodsService;
        private readonly ILogger<ManufacturerGoodsController> _logger;

        public ManufacturerGoodsController(IManufacturerGoodsService manufacturerGoodsService, ILogger<ManufacturerGoodsController> logger)
        {
            _manufacturerGoodsService = manufacturerGoodsService;
            _logger = logger;
        }

        /// <summary>
        ///     Create manufacturer good.
        /// </summary>
        /// <param name="dto">Manufacturer good object</param>
        /// <returns></returns>
        [HttpPost, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<ManufacturerGoods>> Post(CreateManufacturerGoodDto dto)
        {
            return await this._manufacturerGoodsService.Post(dto, Response);
        }

        /// <summary>
        ///     Get manufacturer good by id.
        /// </summary>
        /// <param name="id">Manufacturer good id</param>
        /// <returns></returns>
        [HttpGet("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<ManufacturerGoods>> Get(int id)
        {
            return await this._manufacturerGoodsService.Get(id, Response);
        }

        /// <summary>
        ///     Get all manufacturers goods.
        /// </summary>
        /// <returns></returns>
        [HttpGet, Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ResponseDto<ManufacturerGoods>> GetAll()
        {
            return await this._manufacturerGoodsService.GetAll(Response);
        }

        /// <summary>
        ///    Update manufacturer good.
        /// </summary>
        /// <param name="id">Manufacturer good id</param>
        /// <param name="dto">Manufacturer good object</param>
        /// <returns></returns>
        [HttpPut("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<ManufacturerGoods>> Put(int id, UpdateManufacturerGoodDto dto)
        {
            if (id != dto.Id)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = new List<ManufacturerGoods>(),
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Id in url and id in body are not the same"
                };
            }
            return await this._manufacturerGoodsService.Put(dto, Response);
        }

        /// <summary>
        ///    Delete manufacturer good.
        /// </summary>
        /// <param name="id">Manufacturer good id</param>
        /// <returns></returns>
        [HttpDelete("{id:int}"), Authorize]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<ManufacturerGoods>> Delete(int id)
        {
            return await this._manufacturerGoodsService.Delete(id, Response);
        }
    }
}
