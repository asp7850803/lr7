﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using Serilog.Context;
using System.Diagnostics;
using WebApplication20.Models.Dto;
using WebApplication20.Services.Auth;
using WebApplication20.Telemetry;

namespace WebApplication20.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IAuthService authService, ILogger<AuthController> logger) {
            _authService = authService;
            _logger = logger;
        }

        /// <summary>
        ///    Registration.
        /// </summary>
        /// <param name="dto">User object</param>
        /// <returns></returns>
        [HttpPost("/register")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ResponseDto<AuthResponseDto>> Registration(CreateUserDto dto)
        {
            using var activity = TelemetrySource.WebApiActivitySource.StartActivity("Starting registration activity");
            activity.SetTag("operation.email", dto.Email);
            activity.SetTag("operation.dateOfBirth", dto.DateOfBirth);
            Log.Information("User register with such credentials: {@dto}", dto);
            var result = await this._authService.Register(dto, Response);
            activity.AddEvent(new ActivityEvent("End registration process", DateTimeOffset.Now, new ActivityTagsCollection() { { "Result", result } }));
            return result;
        }

        /// <summary>
        ///    Authorization.
        /// </summary>
        /// <param name="dto">User object</param>
        /// <returns></returns>
        [HttpPost("/login")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ResponseDto<AuthResponseDto>> Login(LoginDto dto)
        {
            using var activity = TelemetrySource.WebApiActivitySource.StartActivity("Starting log in activity");
            activity.SetTag("operation.email", dto.Email);
            activity.SetTag("operation.password", dto.Password);
            Log.Information("User login with such credentials: {@dto}", dto);
            var result =  await this._authService.Login(dto, Response);
            activity.AddEvent(new ActivityEvent("End log in process", DateTimeOffset.Now, new ActivityTagsCollection() { { "Result", result } }));
            return result;
        }
    }
}
