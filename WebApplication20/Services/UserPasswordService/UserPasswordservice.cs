﻿using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using WebApplication20.Data.DatabaseContext;
using WebApplication20.Models.Database;

namespace WebApplication20.Services.UserPasswordService
{
    public class UserPasswordservice:IUserPasswordService
    {

        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using var hmac = new HMACSHA512();

            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        }

        public async void  SetUserHashPassword(UserData user, string password)
        {
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
        }

        public bool VerifyPassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {

            using var hmac = new HMACSHA512(passwordSalt);
            var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

            return computedHash.SequenceEqual(passwordHash);
        }
    }
}
