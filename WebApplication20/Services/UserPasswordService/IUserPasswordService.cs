﻿using WebApplication20.Models.Database;

namespace WebApplication20.Services.UserPasswordService
{
    public interface IUserPasswordService
    {
        public void SetUserHashPassword(UserData user, string password);
        public void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt);
        public bool VerifyPassword(string password, byte[] passwordHash, byte[] passwordSalt);
    }
}
