﻿using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.ManufacturerService
{
    public interface IManufacturerService
    {
        public Task<ResponseDto<Manufacturer>> Get(int id, HttpResponse httpResponse);
        public Task<ResponseDto<Manufacturer>> GetAll(HttpResponse httpResponse);
        public Task<ResponseDto<Manufacturer>> Post(CreateManufacturerDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<Manufacturer>> Put(UpdateManufacturerDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<Manufacturer>> Delete(int id, HttpResponse httpResponse);
        public Task<Manufacturer?> CheckConnection();
    }
}
