﻿using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web.Http;
using WebApplication20.Data.DatabaseContext;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Services.ManufacturerService
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly DataContext _dataContext;
        private readonly IQueueService _queueService;

        public ManufacturerService(DataContext dataContext, IQueueService queueService)
        {
            _dataContext = dataContext;
            _queueService = queueService;
        }

        public async Task<ResponseDto<Manufacturer>> Delete(int Id, HttpResponse httpResponse)
        {
            try
            {
       
                var deletedManufacturer = await _dataContext.Manufacturers.FindAsync(Id);
                if (deletedManufacturer == null)
                {
                    throw new Exception("Element does not exist");
                }
                _dataContext.Manufacturers.Remove(deletedManufacturer);
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<Manufacturer>
                {
                    Data = new List<Manufacturer> { deletedManufacturer },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<Manufacturer>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<Manufacturer>()
                };
            }
        }

        public async Task<ResponseDto<Manufacturer>> Get(int Id, HttpResponse httpResponse)
        {
            try
            {
                var result = await _dataContext.Manufacturers.FindAsync(Id);
                if (result == null)
                {
                    throw new Exception("Element does not exist");
                }
                return new ResponseDto<Manufacturer>
                {
                    Data = new List<Manufacturer> { result },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<Manufacturer>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<Manufacturer>()
                };
            }
        }

        public async Task<ResponseDto<Manufacturer>> GetAll(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                return new ResponseDto<Manufacturer>
                {
                    Data = _dataContext.Manufacturers.ToList(),
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                return new ResponseDto<Manufacturer>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<Manufacturer>()
                };
            }
        }

        public async Task<ResponseDto<Manufacturer>> Post(CreateManufacturerDto dto, HttpResponse httpResponse)
        {
            try
            {
                Manufacturer newManufacturer = new Manufacturer { Name=dto.Name, Founder=dto.Founder, Country=dto.Country};
                _dataContext.Manufacturers.Add(newManufacturer);
                await _dataContext.SaveChangesAsync();
                DbChangesQueue dbChangesQueue = new DbChangesQueue()
                {
                    Id = newManufacturer.Id,
                    TableName = "Manifacturers"
                };
                this._queueService.AddInfo(dbChangesQueue);
                return new ResponseDto<Manufacturer>
                {
                    Data = new List<Manufacturer> { newManufacturer },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                return new ResponseDto<Manufacturer>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<Manufacturer>()
                };
            }
        }

        public async Task<ResponseDto<Manufacturer>> Put(UpdateManufacturerDto dto, HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                var updatedManufacturer = await _dataContext.Manufacturers.FindAsync(dto.Id);
                if (updatedManufacturer == null)
                {
                    throw new Exception("Element does not exist");
                }
                if (dto.Name != null)
                {
                    updatedManufacturer.Name = dto.Name;
                }
                if (dto.Country != null)
                {
                    updatedManufacturer.Country = dto.Country;
                }
               if(dto.Founder!= null)
                {
                    updatedManufacturer.Founder = dto.Founder;
                }
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<Manufacturer>
                {
                    Data = new List<Manufacturer> { updatedManufacturer },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<Manufacturer>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<Manufacturer>()
                };
            }
        }

        public async Task<Manufacturer?> CheckConnection()
        {
            try
            {
                var data = await _dataContext.Manufacturers.FirstOrDefaultAsync();
                return data == null ? null : data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
