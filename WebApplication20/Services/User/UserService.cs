﻿using Humanizer;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Net;
using WebApplication20.Data.DatabaseContext;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.Auth;
using WebApplication20.Services.QueueService;
using WebApplication20.Services.UserPasswordService;

namespace WebApplication20.Services.User
{
    public class UserService : IUserService
    {

        private readonly DataContext _dataContext;
        private IUserPasswordService _userPasswordService;
        private readonly IQueueService _queueService;

        public UserService(IUserPasswordService userPasswordService, DataContext dataContext, IQueueService queueService)
        {
            _userPasswordService = userPasswordService;
            _dataContext = dataContext;
            _queueService = queueService;
        }

        public async Task<ResponseDto<UserData>> Delete(int Id, HttpResponse httpResponse)
        {
            try
            {
                var deletedUser = await _dataContext.UsersData.FindAsync(Id);
                if (deletedUser == null)
                {
                    throw new Exception("Element does not exist");
                }
                _dataContext.UsersData.Remove(deletedUser);
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<UserData>
                {
                    Data = new List<UserData> { deletedUser },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<UserData>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<UserData>()
                };
            }
        }

        public async Task<ResponseDto<UserData>> Get(int Id, HttpResponse httpResponse)
        {
            try
            {
                var user = await _dataContext.UsersData.FindAsync(Id);
                if(user == null)
                {
                    new Exception("User does not exist");
                }
                return new ResponseDto<UserData>
                {
                    Data = new List<UserData> { user },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            } catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<UserData>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<UserData>()
                };
            }
        }

        public async Task<ResponseDto<UserData>> GetAll(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                return new ResponseDto<UserData>
                {
                    Data = _dataContext.UsersData.ToList(),
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<UserData>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<UserData>()
                };
            }
        }

        public async Task<UserData> GetByEmail(string Email)
        {
            Console.WriteLine(Email); 
                var user = await _dataContext.UsersData.FirstOrDefaultAsync((element)=>element.Email==Email);
            Console.WriteLine(user);
                return user;           
        }

        public async Task<UserData> Post(CreateUserDto dto)
        {
            UserData user = new UserData { DateOfBirth= dto.DateOfBirth, Email=dto.Email, FirstName=dto.FirstName, InvalidLoginAttempts=0, IsLocked=false, LastAuthorize=DateTime.Now, LastName=dto.LastName, RoleId=2};
            _dataContext.UsersData.Add(user);
            _userPasswordService.SetUserHashPassword(user, dto.Password);
            await _dataContext.SaveChangesAsync();
            DbChangesQueue dbChangesQueue = new DbChangesQueue()
            {
                Id = user.Id,
                TableName = "Users"
            };
            this._queueService.AddInfo(dbChangesQueue);
            return user;
        }

        public async Task<ResponseDto<UserData>> Put( UpdateUserDto dto, HttpResponse httpResponse)
        {
            try
            {
                var updatedUser = await _dataContext.UsersData.FindAsync(dto.Id);
                if (updatedUser == null)
                {
                    throw new Exception("Element does not exist");
                }
                if(dto.FirstName!=null)updatedUser.FirstName = dto.FirstName;
                if (dto.LastName!=null)updatedUser.LastName = dto.LastName;
                if (dto.Email!=null)updatedUser.Email = dto.Email;
                if(dto.DateOfBirth!=null)updatedUser.DateOfBirth = (DateTime)dto.DateOfBirth;
                if(dto.RoleId!=null)updatedUser.RoleId = (int)dto.RoleId;
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<UserData>
                {
                    Data = new List<UserData> { updatedUser },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<UserData>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<UserData>()
                };
            }
        }

        public async Task<UserData?> CheckConnection()
        {
            try
            {
                var user = await _dataContext.UsersData.FirstOrDefaultAsync();
                return user == null ? null : user;
            } catch (Exception ex)
            {
                return null;
            }
        }
    }
}
