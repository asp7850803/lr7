﻿using Microsoft.AspNetCore.Http;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.User
{
    public interface IUserService
    {
        public Task<ResponseDto<UserData>> Get(int id, HttpResponse httpResponse);
        public Task<ResponseDto<UserData>> GetAll(HttpResponse httpResponse);
        public Task<ResponseDto<UserData>> Delete(int id, HttpResponse httpResponse);
        public Task<ResponseDto<UserData>> Put( UpdateUserDto dto,HttpResponse httpResponse);
        public Task<UserData> GetByEmail(string email);
        public Task<UserData> Post(CreateUserDto dto);
        public Task<UserData?> CheckConnection();
    }
}
