﻿using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.GoodsService
{
    public interface IGoodsService
    {
        public Task<ResponseDto<Goods>> Get(int id, HttpResponse httpResponse);
        public Task<ResponseDto<Goods>> GetAll(HttpResponse httpResponse);
        public Task<ResponseDto<Goods>> Post(CreateGoodDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<Goods>> Put(UpdateGoodDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<Goods>> Delete(int id, HttpResponse httpResponse);
        public Task<Goods?> CheckConnection();
    }
}
