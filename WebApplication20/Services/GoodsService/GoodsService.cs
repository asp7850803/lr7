﻿using Microsoft.EntityFrameworkCore;
using System.Net;
using WebApplication20.Data.DatabaseContext;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Services.GoodsService
{
    public class GoodsService : IGoodsService
    {
        private readonly DataContext _dataContext;
        private readonly IQueueService _queueService;

        public GoodsService(DataContext dataContext, IQueueService queueService)
        {
            _dataContext = dataContext;
            _queueService = queueService;
        }

        public async Task<ResponseDto<Goods>> Delete(int Id, HttpResponse httpResponse)
        {
            try
            {
                var deletedGood = await _dataContext.Goods.FindAsync(Id);
                if(deletedGood == null)
                {
                    throw new Exception("Element does not exist");
                }
                _dataContext.Goods.Remove(deletedGood);
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<Goods>
                {
                    Data = new List<Goods> { deletedGood },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            } catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<Goods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<Goods>()
                };
            }
        }

        public async Task<ResponseDto<Goods>> Get(int Id, HttpResponse httpResponse)
        {
            try {
                var result = await _dataContext.Goods.FindAsync(Id);
                if (result == null)
                {
                    throw new Exception("Element does not exist");
                }
                return new ResponseDto<Goods>
                {
                    Data = new List<Goods> { result },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<Goods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<Goods>()
                };
            }
        }

        public async Task<ResponseDto<Goods>> GetAll(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                var goods = _dataContext.Goods.ToList();
                return new ResponseDto<Goods>
                {
                    Data = goods,
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                return new ResponseDto<Goods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<Goods>()
                };
            }

        }

        public async Task<ResponseDto<Goods>> Post(CreateGoodDto dto, HttpResponse httpResponse)
        {
            try
            {
                Goods newGood = new Goods{ Name = dto.Name,  Type = dto.Type};
                _dataContext.Goods.Add(newGood);
                await _dataContext.SaveChangesAsync();
                DbChangesQueue dbChangesQueue = new DbChangesQueue()
                {
                    Id = newGood.Id,
                    TableName="Goods"
                };
                this._queueService.AddInfo(dbChangesQueue);
                return new ResponseDto<Goods>
                {
                    Data = new List<Goods> { newGood },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                return new ResponseDto<Goods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<Goods>()
                };
            }
        }

        public async Task<ResponseDto<Goods>> Put(UpdateGoodDto dto, HttpResponse httpResponse)
        {
            try
            {
                var updatedGood = await _dataContext.Goods.FindAsync(dto.Id);
                if (updatedGood == null)
                {
                    throw new Exception("Element does not exist");
                }
                if (dto.Name != null)
                {
                    updatedGood.Name = dto.Name;
                }
                if (dto.Type != null)
                {
                    updatedGood.Type = dto.Type;
                }
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<Goods>
                {
                    Data = new List<Goods> { updatedGood },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<Goods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<Goods>()
                };
            }
        }

        public async Task<Goods?> CheckConnection()
        {
            try
            {
                var data = await _dataContext.Goods.FirstOrDefaultAsync();
                return data == null ? null : data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
