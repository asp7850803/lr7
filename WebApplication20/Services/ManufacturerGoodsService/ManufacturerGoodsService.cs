﻿using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web.Http;
using WebApplication20.Data.DatabaseContext;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.GoodsService;
using WebApplication20.Services.ManufacturerService;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Services.ManufacturerGoodsService
{
    public class ManufacturerGoodsService : IManufacturerGoodsService
    {
        private readonly IManufacturerService _manufacturerService;
        private readonly IGoodsService _goodsService;
        private readonly DataContext _dataContext;
        private readonly IQueueService _queueService;

        public ManufacturerGoodsService(IQueueService queueService, IManufacturerService manufacturerService, IGoodsService goodsService, DataContext dataContext)
        {
            _manufacturerService = manufacturerService;
            _goodsService = goodsService;
            _dataContext = dataContext;
            _queueService = queueService;
        }



        public async Task<ResponseDto<ManufacturerGoods>> Delete(int Id, HttpResponse httpResponse)
        {
            try
            {
                var deletedManufacturerGood = await _dataContext.ManufacturerGoods.FindAsync(Id);
                if (deletedManufacturerGood == null)
                {
                    throw new Exception("Element does not exist");
                }
                _dataContext.ManufacturerGoods.Remove(deletedManufacturerGood);
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = new List<ManufacturerGoods> { deletedManufacturerGood },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (HttpResponseException ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<ManufacturerGoods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<ManufacturerGoods>()
                };
            }
        }

        public async Task<ResponseDto<ManufacturerGoods>> Get(int Id, HttpResponse httpResponse)
        {
            try
            {
                var result = await _dataContext.ManufacturerGoods.FindAsync(Id);
                if (result == null)
                {
                    throw new Exception("Element does not exist");
                }
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = new List<ManufacturerGoods> { result },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<ManufacturerGoods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<ManufacturerGoods>()
                };
            }
        }

        public async Task<ResponseDto<ManufacturerGoods>> GetAll(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = _dataContext.ManufacturerGoods.ToList(),
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                return new ResponseDto<ManufacturerGoods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<ManufacturerGoods>()
                };
            }
        }

        public async Task<ResponseDto<ManufacturerGoods>> Post(CreateManufacturerGoodDto dto, HttpResponse httpResponse)
        {
            try
            {
                var manufacturer = await this._manufacturerService.Get(dto.ManufacturerId, httpResponse);
                if (httpResponse.StatusCode== (int)HttpStatusCode.BadRequest)
                {
                    throw new Exception("Manufacturer not found");
                }
                var good = await this._goodsService.Get(dto.GoodId, httpResponse);
                if (httpResponse.StatusCode == (int)HttpStatusCode.BadRequest)
                {
                    throw new Exception("Good not found");
                }
            
                ManufacturerGoods newManufacturerGood = new ManufacturerGoods { GoodId=dto.GoodId, ManufacturerId=dto.ManufacturerId, Description=dto.Description, Price=dto.Price};
                _dataContext.ManufacturerGoods.Add(newManufacturerGood);
                await _dataContext.SaveChangesAsync();
                DbChangesQueue dbChangesQueue = new DbChangesQueue()
                {
                    Id = newManufacturerGood.Id,
                    TableName = "Manufacturer goods"
                };
                this._queueService.AddInfo(dbChangesQueue);
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = new List<ManufacturerGoods> { newManufacturerGood },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<ManufacturerGoods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<ManufacturerGoods>()
                };
            }
        }

        public async Task<ResponseDto<ManufacturerGoods>> Put(UpdateManufacturerGoodDto dto, HttpResponse httpResponse)
        {
            try
            {
                var updatedManufacturerGood = await _dataContext.ManufacturerGoods.FindAsync(dto.Id);
                if (updatedManufacturerGood == null)
                {
                    throw new Exception("Element does not exist");
                }
                var manufacturer = await this._manufacturerService.Get(dto.ManufacturerId, httpResponse);
                if (httpResponse.StatusCode == (int)HttpStatusCode.BadRequest)
                {
                    throw new Exception("Manufacturer not found");
                }
                var good = await this._goodsService.Get(dto.GoodId, httpResponse);
                if (httpResponse.StatusCode == (int)HttpStatusCode.BadRequest)
                {
                    throw new Exception("Good not found");
                }
                if (dto.ManufacturerId != null)
                {
                    updatedManufacturerGood.ManufacturerId = dto.ManufacturerId;
                }
                if (dto.Price != null)
                {
                    updatedManufacturerGood.Price = (double)dto.Price;
                }
                if (dto.Description != null)
                {
                    updatedManufacturerGood.Description = dto.Description;
                }
                if (dto.GoodId != null)
                {
                    updatedManufacturerGood.GoodId = dto.GoodId;
                }
                await _dataContext.SaveChangesAsync();
                return new ResponseDto<ManufacturerGoods>
                {
                    Data = new List<ManufacturerGoods> { updatedManufacturerGood },
                    StatusCode = HttpStatusCode.Created,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseDto<ManufacturerGoods>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<ManufacturerGoods>()
                };
            }
        }

        public async Task<ManufacturerGoods?> CheckConnection()
        {
            try
            {
                var data = await _dataContext.ManufacturerGoods.FirstOrDefaultAsync();
                return data == null ? null : data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
