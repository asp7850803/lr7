﻿using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.ManufacturerGoodsService
{
    public interface IManufacturerGoodsService
    {
        public Task<ResponseDto<ManufacturerGoods>> Get(int id, HttpResponse httpResponse);
        public Task<ResponseDto<ManufacturerGoods>> GetAll(HttpResponse httpResponse);
        public Task<ResponseDto<ManufacturerGoods>> Post(CreateManufacturerGoodDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<ManufacturerGoods>> Put(UpdateManufacturerGoodDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<ManufacturerGoods>> Delete(int id, HttpResponse httpResponse);
        public Task<ManufacturerGoods?> CheckConnection();
    }
}
