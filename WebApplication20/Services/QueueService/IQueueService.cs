﻿using WebApplication20.Models.Database;

namespace WebApplication20.Services.QueueService
{
    public interface IQueueService
    {
        public int GetLength();
        public void AddInfo(DbChangesQueue data);
        public DbChangesQueue RemoveInfo();
    }
}
