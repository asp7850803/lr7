﻿using WebApplication20.Models.Database;

namespace WebApplication20.Services.QueueService
{
    public class QueueService : IQueueService
    {
        private readonly Queue<DbChangesQueue> _dbChangesQueues = new Queue<DbChangesQueue>();

        public void AddInfo(DbChangesQueue data)
        {
            this._dbChangesQueues.Enqueue(data);
        }

        public int GetLength()
        {
            return this._dbChangesQueues.Count;
        }

        public DbChangesQueue RemoveInfo()
        {
            return this._dbChangesQueues.Dequeue();
        }
    }
}
