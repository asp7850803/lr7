﻿using System.Net;
using System.Security.Cryptography;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.User;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using WebApplication20.Services.UserPasswordService;
using WebApplication20.Data.DatabaseContext;
using Serilog;

namespace WebApplication20.Services.Auth
{
    public class AuthService : IAuthService
    {
        private IUserService _userService;
        private IConfiguration _configuration;
        private IUserPasswordService _userPasswordService;
        private readonly DataContext _dataContext;

        public AuthService(IConfiguration config, IUserService userService, IUserPasswordService userPasswordService, DataContext dataContext) {
            _configuration = config;
            _userService = userService;
            _userPasswordService = userPasswordService;
            _dataContext = dataContext;
        }

        

        public string CreateToken(UserData user)
        {
            var userRole = _dataContext.UserRoles.FirstOrDefault((element)=>element.Id==user.RoleId);
            List<Claim> claims = new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, userRole.Name)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetValue<string>("Auth:Key")));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: creds
            );
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt;
        }

        public async Task<ResponseDto<AuthResponseDto>> Login(LoginDto dto, HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                var user = await _userService.GetByEmail(dto.Email);
                if (user == null)
                {
                    httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                    throw new Exception("User with such email does not exist");
                }
                if (user.IsLocked==true)
                {
                    Log.Warning("User account is locked");
                    httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                    throw new Exception("Authentication failed");
                }
                if(!_userPasswordService.VerifyPassword(dto.Password, user.PasswordHash, user.PasswordSalt))
                {
                    user.InvalidLoginAttempts += 1;
                    if(user.InvalidLoginAttempts>= _configuration.GetValue<int>("Auth:MaxLoginAttempts"))
                    {
                        user.IsLocked = true;
                    }
                    httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                    throw new Exception("Authentication failed");
                }
                AuthResponseDto authResponseDto = new AuthResponseDto(CreateToken(user));
                user.LastAuthorize=DateTime.Now;
                Log.Debug("User credentials: @user", user);
                return new ResponseDto<AuthResponseDto>
                {
                    Data = new List<AuthResponseDto> { authResponseDto },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            } catch (Exception ex)
            {
                Log.Error("User with such credentials not found");
                return new ResponseDto<AuthResponseDto>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.NotFound,
                    Data = new List<AuthResponseDto>()
                };
            }
        }

        public async Task<ResponseDto<AuthResponseDto>> Register(CreateUserDto dto, HttpResponse httpResponse)
        {
            try
            {
                var userInDb = await this._userService.GetByEmail(dto.Email);
                if (userInDb != null)
                {
                    httpResponse.StatusCode = (int)HttpStatusCode.BadRequest;
                    throw new Exception("User with such email already exist");
                }
                var user = await this._userService.Post(dto);
                AuthResponseDto authResponseDto = new AuthResponseDto(CreateToken(user));
                Log.Debug("User credentials: @user", user);
                return new ResponseDto<AuthResponseDto>
                {
                    Data = new List<AuthResponseDto> { authResponseDto },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            } catch (Exception ex)
            {
                Log.Error("User with such email already exis");
                return new ResponseDto<AuthResponseDto>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.BadRequest,
                    Data = new List<AuthResponseDto>()
                };
            }
        }
    }
}
