﻿using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.Auth
{
    public interface IAuthService
    {

        public Task<ResponseDto<AuthResponseDto>> Register(CreateUserDto dto, HttpResponse httpResponse);
        public Task<ResponseDto<AuthResponseDto>> Login(LoginDto dto, HttpResponse httpResponse);
        public string CreateToken(UserData user);
    }
}
