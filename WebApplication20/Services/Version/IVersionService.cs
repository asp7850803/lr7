﻿using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using DocumentFormat.OpenXml.Spreadsheet;
using ClosedXML.Excel;

namespace WebApplication20.Services.Version
{
    public interface IVersionService
    {
        public Task<ResponseDto<int>> Get(HttpResponse httpResponse);
        public Task<ResponseDto<string>> GetV2(HttpResponse httpResponse);
        public Task<byte[]> GetV3(HttpResponse httpResponse);
    }
}
