﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Net;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;

namespace WebApplication20.Services.Version
{
    public class VersionService : IVersionService
    {
        public async Task<ResponseDto<int>> Get(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                Random random = new Random();
                int randomNumber = random.Next();
                Console.WriteLine(randomNumber);
                return new ResponseDto<int>
                {
                    Data = new List<int> { randomNumber },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<int>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<int>()
                };
            }
        }

        public async Task<ResponseDto<string>> GetV2(HttpResponse httpResponse)
        {
            try
            {
                await Task.Delay(1000);
                string message = "Yevhenii Stoev is my favourite teacher";
                return new ResponseDto<string>
                {
                    Data = new List<string> { message },
                    StatusCode = HttpStatusCode.OK,
                    Message = "Done"
                };
            }
            catch (Exception ex)
            {
                httpResponse.StatusCode = (int)HttpStatusCode.NotFound;
                return new ResponseDto<string>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<string>()
                };
            }
        }

        public async Task<byte[]> GetV3(HttpResponse httpResponse)
        {
                await Task.Delay(1000);
                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("Sample Sheet");
                    worksheet.Cell("A1").Value = "Hello World!";
                    worksheet.Cell("A2").FormulaA1 = "=MID(A1, 7, 5)";

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    return stream.ToArray();
                }

            }

        }
    }
}
