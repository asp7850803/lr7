﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using WebApplication20.Data.DatabaseContext;

namespace WebApplication20.Health
{
    public class DatabaseHealthCheck : IHealthCheck
    {
        private readonly DataContext _dataContext;

        public DatabaseHealthCheck(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _dataContext.Database.CanConnectAsync(cancellationToken);
                if(!result)
                {
                    throw new Exception("Database is not available");
                }
                Log.Information("Database is available");
                return HealthCheckResult.Healthy("Database is available");
            }catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(exception: ex);
            }
        }
    }
}
