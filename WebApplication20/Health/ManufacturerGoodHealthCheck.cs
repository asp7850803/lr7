﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using System.Net;
using WebApplication20.Services.GoodsService;
using WebApplication20.Services.ManufacturerGoodsService;

namespace WebApplication20.Health
{
    public class ManufacturerGoodHealthCheck : IHealthCheck
    {
        private readonly IManufacturerGoodsService _manufacturerGoodsService;

        public ManufacturerGoodHealthCheck(IManufacturerGoodsService manufacturerGoodsService)
        {
            _manufacturerGoodsService = manufacturerGoodsService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _manufacturerGoodsService.CheckConnection();
                if (result == null)
                {
                    throw new Exception("ManufacturerGood service is not available");
                }
                Log.Information("ManufacturerGood service is available");
                return HealthCheckResult.Healthy("ManufacturerGood service is available");
            } catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(exception: ex);
            }
        }
    }
}
