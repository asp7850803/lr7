﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using System.Net;
using WebApplication20.Services.GoodsService;
using WebApplication20.Services.User;

namespace WebApplication20.Health
{
    public class UserHealthCheck : IHealthCheck
    {
        private readonly IUserService _userService;

        public UserHealthCheck(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _userService.CheckConnection();
                if (result == null)
                {
                    throw new Exception("Users service is not available");
                }
                Log.Information("Users service is available");
                return HealthCheckResult.Healthy("Users service is available");
            }
            catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(exception: ex);
            }
        }
    }
}
