﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using System.Net;
using WebApplication20.Services.GoodsService;
using WebApplication20.Services.ManufacturerService;

namespace WebApplication20.Health
{
    public class ManufacturerHealthCheck : IHealthCheck
    {
        private readonly IManufacturerService _manufacturerService;

        public ManufacturerHealthCheck(IManufacturerService manufacturerService)
        {
            _manufacturerService = manufacturerService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _manufacturerService.CheckConnection();
                if (result == null)
                {
                    throw new Exception("Manufacturer service is not available");
                }
                Log.Information("Manufacturer service is available");
                return HealthCheckResult.Healthy("Manufacturer service is available");
            }
            catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(exception: ex);
            }
        }
    }
}
