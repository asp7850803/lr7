﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using System.Net;
using WebApplication20.Services.GoodsService;

namespace WebApplication20.Health
{
    public class GoodHealthCheck : IHealthCheck
    {
        private readonly IGoodsService _goodsService;

        public GoodHealthCheck(IGoodsService goodsService) {  
            _goodsService = goodsService;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await _goodsService.CheckConnection();
                if (result == null)
                {
                    throw new Exception("Goods service is not available");
                }
                Log.Information("Good service is available");
                return HealthCheckResult.Healthy("Goods service is available");
            } catch (Exception ex)
            {
                return HealthCheckResult.Unhealthy(exception: ex);
            }
        }
    }
}
