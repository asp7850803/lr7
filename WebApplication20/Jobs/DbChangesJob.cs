﻿
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Net.Http;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Jobs
{
    public class DbChangesJob : BackgroundService
    {
        private readonly IQueueService _queueService;
        private readonly SendGridClient _client;
        public DbChangesJob(IQueueService queueService, IConfiguration configuration)
        {
            _queueService = queueService;
            _client = new SendGridClient(configuration.GetValue<string>("Sendgrid"));
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (this._queueService.GetLength() > 0)
                {
                    var data = this._queueService.RemoveInfo();
                    var subject = "Added new entity to db";
                    var textContent = $"Added to table:{data.TableName} with id:{data.Id}";
                    var msg = MailHelper.CreateSingleEmail(new EmailAddress("evgenkitan@gmail.com"), new EmailAddress("evgenkitan@gmail.com"), subject, textContent, null);
                    await _client.SendEmailAsync(msg);
                }
                await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
            }
        }
    }
}
