﻿
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System.Net.Http;

namespace WebApplication20.Jobs
{
    public class PageHealthcheckJob : BackgroundService
    {
        private readonly HttpClient _httpClient;
        private readonly string _urlToCheck;

        public PageHealthcheckJob()
        {
            _httpClient = new HttpClient();
            _urlToCheck = "https://hay-haul-landing.vercel.app/";
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    HttpResponseMessage response = await _httpClient.GetAsync(_urlToCheck);
                    bool isPageAvailable = response.IsSuccessStatusCode;
                    this.LogResultToFile($"Page health check result - URL: {_urlToCheck}, Available: {isPageAvailable}");

                    await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
                }
                catch (Exception ex)
                {
                    this.LogResultToFile($"Error occurred during page health check, error: {ex}");
                }
            }
        }

        private void LogResultToFile(string logMessage)
        {
            try
            {
                using (StreamWriter sw = File.AppendText("logs/bgLogs.txt"))
                {
                    sw.WriteLine(logMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Failed to log result to file");
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            this.LogResultToFile("Service stopped");
            return base.StopAsync(cancellationToken);
        }
    }
}
