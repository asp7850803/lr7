﻿using Humanizer;
using Quartz;
using SendGrid;
using SendGrid.Helpers.Mail.Model;
using SendGrid.Helpers.Mail;

namespace WebApplication20.Jobs
{
    [DisallowConcurrentExecution]
    public class QuartzJob : IJob
    {
        private readonly SendGridClient _client;

        public QuartzJob(IConfiguration configuration)
        {
            _client = new SendGridClient(configuration.GetValue<string>("Sendgrid"));
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var subject = "Server is running";
            var textContent = "Your web api server is running";
            var msg = MailHelper.CreateSingleEmail(new EmailAddress("evgenkitan@gmail.com"), new EmailAddress("evgenkitan@gmail.com"), subject, textContent, null);
            await _client.SendEmailAsync(msg);
        }
    }
}
