﻿
using Microsoft.Extensions.Caching.Memory;
using System.Text.Json;
using WebApplication20.Models.Dto;

namespace WebApplication20.Jobs
{
    public class CacheCurrenciesJob : BackgroundService
    {
        private readonly HttpClient _client;
        private readonly string _apiKey;
        private readonly IMemoryCache _memoryCache;
        private readonly string cacheKey = "currencies";

        public CacheCurrenciesJob(IHttpClientFactory httpClientFactory, IConfiguration configuration, IMemoryCache memoryCache)
        {
            _client = httpClientFactory.CreateClient();
            _client.BaseAddress = new Uri("https://api.currencybeacon.com/v1/latest");
            _apiKey = configuration.GetValue<string>("APIKey");
            _memoryCache = memoryCache;
        }


        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                APIResponseDto responseDto;
                if(!_memoryCache.TryGetValue(cacheKey, out responseDto))
                {
                    var response = await _client.GetAsync($"?api_key={_apiKey}&base=UAH");
                    response.EnsureSuccessStatusCode();

                    var responseData = await response.Content.ReadAsStringAsync();
                    responseDto = JsonSerializer.Deserialize<APIResponseDto>(responseData);
                    _memoryCache.Set(cacheKey, responseDto,
                    new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromMinutes(10)));
                }
                await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
            }

        }
    }
}
