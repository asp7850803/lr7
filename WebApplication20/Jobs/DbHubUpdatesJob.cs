﻿
using Microsoft.AspNetCore.SignalR;
using SendGrid.Helpers.Mail;
using WebApplication20.Hubs;
using WebApplication20.Models.Database;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Jobs
{
    public class DbHubUpdatesJob : BackgroundService
    {
        private readonly IQueueService _queueService;
        private readonly IHubContext<NewDataInDbHub, INewDataInDbHub> _dbUpdatesHub;

        public DbHubUpdatesJob(IQueueService queueService, IHubContext<NewDataInDbHub, INewDataInDbHub> dbUpdatesHub)
        {
            _queueService = queueService;
            _dbUpdatesHub = dbUpdatesHub;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {

                if (this._queueService.GetLength() > 0)
                {
                    List<DbChangesQueue> dbChangesQueue = new List<DbChangesQueue>();
                    while (this._queueService.GetLength() > 0)
                    {
                        dbChangesQueue.Add(this._queueService.RemoveInfo());
                    }
                    await this._dbUpdatesHub.Clients.All.SendUpdates(dbChangesQueue);
                }
                await Task.Delay(TimeSpan.FromMinutes(10), stoppingToken);
            }
        }
    }
}
