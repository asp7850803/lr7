﻿using System.Diagnostics;

namespace WebApplication20.Telemetry
{
    public class Telemetry
    {
        public static readonly string Name = "WebApi";
        public static readonly string Version = "1.0.0";
        
    }

    public class TelemetrySource
    {
        public static readonly ActivitySource WebApiActivitySource = new(Telemetry.Name);
    }
}
