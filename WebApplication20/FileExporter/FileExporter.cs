﻿using OpenTelemetry;
using System.Diagnostics;

namespace WebApplication20.FileExporter
{
    public class FileExporter : BaseExporter<Activity>
    {
        private readonly string _filePath;

        public FileExporter(string filePath)
        {
            _filePath = filePath;
        }

        public override ExportResult Export(in Batch<Activity> batch)
        {
            using (var streamWriter = new StreamWriter(_filePath, append: true))
            {
                foreach (var activity in batch)
                {
                    streamWriter.WriteLine(activity.DisplayName);
                    streamWriter.WriteLine(activity.StartTimeUtc);
                    streamWriter.WriteLine(activity.Duration);
                }
            }

            return ExportResult.Success;
        }
    }
}
