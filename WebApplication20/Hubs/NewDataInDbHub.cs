﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using WebApplication20.Models.Database;
using WebApplication20.Models.Dto;
using WebApplication20.Services.QueueService;

namespace WebApplication20.Hubs
{
    public interface INewDataInDbHub
    {
        public Task SendUpdates(List<DbChangesQueue> dbChangesQueue);
    }

    public class NewDataInDbHub : Hub<INewDataInDbHub>
    {
        private readonly IQueueService _queueService;

        public NewDataInDbHub(IQueueService queueService)
        {
            _queueService = queueService;
        }

        public async Task GetDbUpdates()
        {
            List<DbChangesQueue> dbChangesQueue = new List<DbChangesQueue>();
            while (this._queueService.GetLength() > 0)
            {
                dbChangesQueue.Add(this._queueService.RemoveInfo());
            }
            await Clients.All.SendUpdates(dbChangesQueue);
        }
    }
}
