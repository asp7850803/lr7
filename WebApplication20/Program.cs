using WebApplication20.Services.GoodsService;
using WebApplication20.Services.ManufacturerGoodsService;
using WebApplication20.Services.ManufacturerService;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using WebApplication20.Services.Auth;
using WebApplication20.Services.User;
using Microsoft.OpenApi.Models;
using System.Reflection;
using WebApplication20.Services.UserPasswordService;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using WebApplication20.Configuration;
using WebApplication20.Services.Version;
using Asp.Versioning;
using WebApplication20.Data.DatabaseContext;
using Microsoft.EntityFrameworkCore;
using WebApplication20.Health;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Serilog;
using Serilog.Events;
using Serilog.Exceptions.Core;
using Serilog.Exceptions.SqlServer.Destructurers;
using Serilog.Exceptions;
using Serilog.Exceptions.EntityFrameworkCore.Destructurers;
using Serilog.Formatting.Display;
using SendGrid;
using Serilog.Sinks.Email;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Logs;
using WebApplication20.Telemetry;
using OpenTelemetry.Trace;
using Microsoft.Identity.Client.TelemetryCore.TelemetryClient;
using OpenTelemetry;
using WebApplication20.FileExporter;
using WebApplication20.Jobs;
using Quartz;
using WebApplication20.Models.Database;
using WebApplication20.Services.QueueService;



try
{
    var builder = WebApplication.CreateBuilder(args);
    var client = new SendGridClient(builder.Configuration.GetSection("Sendgrid").Value);
    var emailConnectionInfo = new EmailConnectionInfo
    {
        EmailSubject = "Log info",
        FromEmail = "evgenkitan@gmail.com",
        ToEmail = "evgenkitan@gmail.com",
        SendGridClient = client,
        FromName = "From Yevhenii"
    };
    Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
            .Enrich.WithClientIp()
            .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
            .WithDefaultDestructurers()
            .WithDestructurers(new[] { new SqlExceptionDestructurer() })
            .WithDestructurers(new[] { new DbUpdateExceptionDestructurer() }))
            .Enrich.WithCorrelationId()
            .WriteTo.Email(emailConnectionInfo, restrictedToMinimumLevel: LogEventLevel.Error)
            .WriteTo.Console(outputTemplate:
        "[{Timestamp:yyy:MM:dd:HH:mm:ss} {Level:u3}] {ClientIp} {CorrelationId} {Message:lj}{NewLine}{Exception} {Properties:j}")
            .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day, outputTemplate:
        "[{Timestamp:yyy:MM:dd:HH:mm:ss} {Level:u3}] {ClientIp} {CorrelationId} {Message:lj}{NewLine}{Exception} {Properties:j}")
            .CreateLogger();
    builder.Logging.AddOpenTelemetry(options =>
    {
        options
        .SetResourceBuilder(
            ResourceBuilder.CreateDefault()
                .AddService(Telemetry.Name, Telemetry.Version));
        options.AddConsoleExporter();
    });
    builder.Services.AddOpenTelemetry().ConfigureResource(resource =>
            resource.AddService(Telemetry.Name, Telemetry.Version)
    ).WithMetrics(options =>
    {
        options.AddAspNetCoreInstrumentation()
        .AddMeter("Microsoft.AspNetCore.Hosting")
        .AddMeter("Microsoft.AspNetCore.Server.Kestrel")
        .AddConsoleExporter()
        .AddOtlpExporter(otlp => { otlp.Endpoint = new Uri("http://localhost:4317"); });
    }).WithTracing(tracing => tracing
        .AddAspNetCoreInstrumentation()
        .AddHttpClientInstrumentation()
        .AddProcessor(new BatchActivityExportProcessor(new FileExporter(
                  "./traces.txt"
              )))

        .AddJaegerExporter()
        .AddConsoleExporter()
        .AddSource(Telemetry.Name)
        .AddOtlpExporter(otlp => { otlp.Endpoint = new Uri("http://localhost:4317"); }));

    builder.Services.AddControllers();
    builder.Services.AddHttpContextAccessor();

    builder.Services.AddApiVersioning(
        options =>
        {
            options.ReportApiVersions = true;
            options.DefaultApiVersion = new ApiVersion(1.0);
        }).AddApiExplorer(
        options =>
        {
            options.GroupNameFormat = "'v'VVV";
            options.SubstituteApiVersionInUrl = true;
        });
    builder.Services.AddDbContext<DataContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("DatabaseConnection"))
    );
    builder.Services.AddHealthChecks()
        .AddCheck<DatabaseHealthCheck>(name: "Database", tags: new string[] { "db", "sql", "sqlserver" }, failureStatus: HealthStatus.Unhealthy)
        .AddCheck<GoodHealthCheck>(name: "Good", tags: new string[] { "service", "good" }, failureStatus: HealthStatus.Unhealthy)
        .AddCheck<ManufacturerGoodHealthCheck>(name: "ManufacturerGood", tags: new string[] { "service", "manufacturerGood" }, failureStatus: HealthStatus.Unhealthy)
        .AddCheck<ManufacturerHealthCheck>(name: "Manufacturer", tags: new string[] { "service", "manufacturer" }, failureStatus: HealthStatus.Unhealthy)
        .AddCheck<UserHealthCheck>(name: "User", tags: new string[] { "service", "user" }, failureStatus: HealthStatus.Unhealthy);
    builder.Services.AddHealthChecksUI((options) =>
    {
        options.AddHealthCheckEndpoint("Health check", "/health");
    });

    builder.Services.AddHealthChecksUI().AddInMemoryStorage();
    builder.Services.AddScoped<IGoodsService, GoodsService>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IManufacturerService, ManufacturerService>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IManufacturerGoodsService, ManufacturerGoodsService>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IAuthService, AuthService>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IUserPasswordService, UserPasswordservice>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IVersionService, VersionService>();// Used this method because new connection must be created on every request
    builder.Services.AddScoped<IUserService, UserService>();// Used this method because new connection must be created on every request
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSingleton<IQueueService, QueueService>();
    builder.Services.AddMemoryCache();

    builder.Services.AddHostedService<PageHealthcheckJob>();
    builder.Services.AddHostedService<DbChangesJob>();
    builder.Services.AddHostedService<CacheCurrenciesJob>();
    builder.Services.AddHostedService<DbHubUpdatesJob>();
    builder.Services.AddSignalR();
    builder.Services.AddQuartz(options =>
    {
        var jobKey = JobKey.Create(nameof(QuartzJob));
        options.AddJob<QuartzJob>(jobKey).AddTrigger(trigger=>trigger.ForJob(jobKey).WithSimpleSchedule(
            schedule=>schedule.WithIntervalInMinutes(5).RepeatForever()));
    });
    builder.Services.AddQuartzHostedService();
    builder.Services.AddSingleton<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();// Used this method because the service userd for every user
    builder.Services.AddSwaggerGen(
        options =>
        {
            options.OperationFilter<SwaggerDefaultValues>();
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
            $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please insert JWT with Bearer into field",
                Scheme = "Bearer",
                BearerFormat = "JWT",
                Name = "Authorization",
                Type = SecuritySchemeType.Http
            });
            options.AddSecurityRequirement(new OpenApiSecurityRequirement {
         {
           new OpenApiSecurityScheme
           {
             Reference = new OpenApiReference
             {
               Type = ReferenceType.SecurityScheme,
               Id = "Bearer"
             },
               Scheme = "oauth2",
               Name = "Bearer",
               In = ParameterLocation.Header
            },

            new List<string>()
          }
            });
        }
    );
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("Auth:Key").Value))
        };
    });


    var app = builder.Build();

    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;
        var context = services.GetRequiredService<DataContext>();
        context.Database.Migrate();
    }

    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            foreach (var description in app.DescribeApiVersions())
            {
                options.SwaggerEndpoint(
                    $"/swagger/{description.GroupName}/swagger.json",
                    description.GroupName);
            }
        });
    }

    app.MapHealthChecks("/health", new HealthCheckOptions
    {
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });

    app.MapHealthChecks("/health/db", new HealthCheckOptions
    {
        Predicate = reg => reg.Tags.Contains("db"),
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });

    app.MapHealthChecks("/health/good", new HealthCheckOptions
    {
        Predicate = reg => reg.Tags.Contains("good"),
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
    app.MapHealthChecks("/health/manufacturerGood", new HealthCheckOptions
    {
        Predicate = reg => reg.Tags.Contains("manufacturerGood"),
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
    app.MapHealthChecks("/health/manufacturer", new HealthCheckOptions
    {
        Predicate = reg => reg.Tags.Contains("manufacturer"),
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
    app.MapHealthChecks("/health/user", new HealthCheckOptions
    {
        Predicate = reg => reg.Tags.Contains("user"),
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
    app.MapHealthChecksUI((options) =>
    {
        options.UIPath = "/dashboard";
    });

    app.UseHttpsRedirection();

    app.UseAuthentication();

    app.UseAuthorization();

    app.MapControllers();


    app.Run();
}
catch (Exception e)
{
    Log.Fatal($"Server has not started with exception {e}");
}